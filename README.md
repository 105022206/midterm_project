# Software Studio 2019 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : [Chatroom]
* Key functions (add/delete)
    1. [和多人一起聊天]
    2. [和單人一對一聊天]
    
* Other functions (add/delete)
    1. [上傳圖片改變頭像]
    2. [上傳檔案連結到對話紀錄]

## Basic Components
|Component|Score|Y|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|N|

## Website Detail Description

# 作品網址：[https://midtermproject-bf91b.firebaseapp.com/]

# Components Description : 
1. [Sign up] : [利用email註冊帳號] 註冊失敗會跳出提示 
2. [Sign in] : [可以選用已經註冊好的email登入或是使用google登入，成功登入的話會
跳轉到聊天室介面]
3. [loguot]  :[點選logout button就可以登出了。]
3. [public chatroom] : [登入之後點選public就可以進入public聊天室，所有註冊過的
user都可以在裡面聊天，如果是手機介面想要返回user介面的話可以點選聊天室右上角
的箭頭。]
4. [1對1聊天] : [可以在左邊的user介面看到所有的user，點一下想要聊天的對象就可以
切換到和她聊天的聊天室，如果是手機介面想要返回user介面的話可以點選聊天室右上
角的箭頭]
5. [RWD] : [網頁可隨device調整大小，用手機看的話可以由卷軸查看內容] 
6. [CSS animation] : 聊天室標題可以由上往下掉




...

# Other Functions Description(1~10%) : 
1. [上傳圖片改變頭像] : [點選上傳照片就可以將圖片上傳(僅限jpg,gif,png圖檔)並改變
    頭像(預設是貓咪)。]
2. [上傳檔案連結到對話紀錄] : [點選迴紋針button就可以上傳檔案連結，檔案會放到fire
    base儲存起來。]
...

## Security Report (Optional)
1. 其他user看不到別人的1對1聊天紀錄，因為每個人都有自己的firebase節點存取自己的
聊天紀錄，要取得對話紀錄是由當前user和對方的user來取得聊天紀錄，如果是其他 user
就只能取得自己的聊天紀錄。

 
